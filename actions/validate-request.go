package validating_request

import (
	"crypto/hmac"
	"crypto/sha1"
	"encoding/hex"
	"io/ioutil"
	"net/http"
	"strings"

	"github.com/gin-gonic/gin"
	"github.com/sirupsen/logrus"
	confLoader "gitlab.com/sapiens-bot/config-loader"
	"gitlab.com/sapiens-bot/errors"
)

var log *logrus.Entry

func init() {
	log = logrus.WithField("system-middleware", "validating")
}

func ValidatingWebhookEvents() gin.HandlerFunc {
	return func(c *gin.Context) {
		defer c.Request.Body.Close()
		payload, err := ioutil.ReadAll(c.Request.Body)
		if err != nil {
			log.Error("read request body got error:", err.Error())

			resp := errors.APINotAuthenticated.Response(nil)
			resp.SetDebugMsg(err.Error())

			c.Abort()
			c.JSON(http.StatusBadRequest, resp)
			return
		}

		//generating signature
		h := hmac.New(sha1.New, []byte(confLoader.Conf.Facebook.AppSecret))
		_, err = h.Write(payload)
		if err != nil {
			log.Error("new sha1 signature got error:", err.Error())

			resp := errors.APINotAuthenticated.Response(nil)
			resp.SetDebugMsg(err.Error())

			c.Abort()
			c.JSON(http.StatusBadRequest, resp)
			return
		}

		expected_signature := h.Sum(nil)
		// log.Infof("------- expected_signature: %x", h.Sum(nil))
		// log.Infof("------- origin signature: %s", c.Request.Header.Get("X-Hub-Signature"))

		//getting client request signature
		signature := c.Request.Header.Get("X-Hub-Signature")
		if len(signature) != 45 && !strings.HasPrefix(signature, "sha1=") {
			log.Error("signature's not valid")

			resp := errors.APINotAuthenticated.Response(nil)
			resp.SetDebugMsg(err.Error())

			c.Abort()
			c.JSON(http.StatusBadRequest, resp)
			return
		}
		signature = strings.TrimPrefix(signature, "sha1=")

		signatureByte, _ := hex.DecodeString(signature)

		if !hmac.Equal(expected_signature, signatureByte) {
			log.Error("The signature is not the same with our expected signature")

			resp := errors.APINotAuthenticated.Response(nil)
			resp.SetDebugMsg("signature's not valid")

			c.Abort()
			c.JSON(http.StatusBadRequest, resp)
			return
		}

		c.Set("payload", string(payload))
		// log.Info("--------- successful")
	}
}
