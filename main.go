package main

import (
	"encoding/json"
	"net/http"

	"github.com/gin-gonic/contrib/cors"
	"github.com/gin-gonic/gin"
	"github.com/gomessenger/messenger"
	"github.com/ngdlong91/cucumbers/dto"
	"github.com/sirupsen/logrus"
	"gitlab.com/sapiens-bot/actions"
	confLoader "gitlab.com/sapiens-bot/config-loader"
	dl "gitlab.com/sapiens-bot/dialogflow-services"
)

var log *logrus.Entry

func init() {
	confLoader.LoadConfig()
	dto.SetDebugResponse(true)
	dl.Dp.Init("newagent-1-688d8", "config-asset/newagent-1-688d8-5e0ece53548f.json", "en", "Asia/Bangkok")

	log = logrus.WithField("system", "chatbot")
}

func main() {
	router := gin.Default()
	router.Use(cors.Default())

	router.GET("/", verifyWebhook())
	router.POST("/", validating_request.ValidatingWebhookEvents(), processWebhook())

	router.Run(":8000")
}

func verifyWebhook() gin.HandlerFunc {
	return func(c *gin.Context) {
		mode := c.Query("hub.mode")
		challenge := c.Query("hub.challenge")
		token := c.Query("hub.verify_token")

		if mode == "subscribe" && token == confLoader.Conf.Facebook.AppToken {
			c.String(http.StatusOK, challenge)
		} else {
			c.String(http.StatusForbidden, "Error, wrong validation token")
		}
	}
}

func processWebhook() gin.HandlerFunc {
	return func(c *gin.Context) {
		var req messenger.Request

		if err := json.Unmarshal([]byte(c.GetString("payload")), &req); err != nil {
			log.Error("json.Unmarshal payload got error: ", err.Error())
			c.String(http.StatusBadRequest, "Invalid request")
			return
		}

		if req.Object == "page" {
			for _, entry := range req.Entry {
				mess := messenger.New(entry.ID, ProcessMessage, ProcessPostBack)
				for _, messaging := range entry.Messaging {
					mess.ProcessMessaging(entry.ID, confLoader.Conf.Facebook.PageToken, &messaging)
				}
			}

			c.String(http.StatusOK, "Your message has got")
		} else {
			c.String(http.StatusBadRequest, "Message not supported")
		}
	}
}
