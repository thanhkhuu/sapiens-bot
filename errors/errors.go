package errors

import (
	"github.com/ngdlong91/cucumbers/dto"
	ierr "github.com/ngdlong91/cucumbers/errs"
)

//region API Error
type APIError int

const APISuccess APIError = 1000

func (e APIError) IsSuccess() bool {
	return e.Val() == APISuccess.Val()
}

func (e APIError) Msg() string {
	message := apiErrMsg[e]
	if message == "" {
		return defaultAPIErrMsg
	}
	return message
}

func (e APIError) Response(data interface{}) dto.Response {
	return dto.Response{StatusCode: e.Val(), Message: e.Msg(), Data: data}
}

func (e APIError) SetMsg(msg string) {
	apiErrMsg[e] = msg
}

func (e APIError) Success() ierr.CustomError {
	return APISuccess
}

func (e APIError) Val() int {
	return int(e)
}

func (e APIError) Debug() bool {
	return true
}

///
var apiErrMsg map[APIError]string

const defaultAPIErrMsg string = "API not working properly"

const (
	APIConnectionFailed APIError = iota + 1100
	APIAPIConnectionTimeout
	APICannotFetchData
	APIServerBusy
	APIInvalidRequestParams
	APICannotCountRecord
	APICannotExecuteAction
	APIRecordNotExist
	APIRecordExist
	APICannotTransferData
	APIRequestPayloadInvalid
	APINotAuthenticated
)

func init() {
	apiErrMsg = make(map[APIError]string)
	APISuccess.SetMsg("Api execute success")

	APIServerBusy.SetMsg("Server busy now. Please try again")
	APICannotFetchData.SetMsg("Cannot fetch data")
	APIInvalidRequestParams.SetMsg("Invalid request params")
	APICannotCountRecord.SetMsg("Cannot count total record")
	APICannotExecuteAction.SetMsg("Cannot execute request")
	APIRecordNotExist.SetMsg("Record not exist")
	APIRecordExist.SetMsg("Record exist")
	APICannotTransferData.SetMsg("Cannot transfer data")
	APIRequestPayloadInvalid.SetMsg("Request payload invalid")
	APINotAuthenticated.SetMsg("Cannot access api. You are not authenticated")
}

//endregion
