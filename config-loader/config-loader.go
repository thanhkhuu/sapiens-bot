package config

import (
	"fmt"
	"os"

	"github.com/fsnotify/fsnotify"
	"github.com/spf13/viper"
)

var Conf Config

type Config struct {
	// Server ServerConf
	Security SecurityConf

	// Db      DbConf
	Version VersionConf
	// Social  SocialConf
	//Api      ApiConf

	//Cache    CacheConf
	//Aws      AwsConf

	//platform
	Facebook FacebookConf
}

//region Conf definition

type SocialConf struct {
	Facebook struct {
		ClientId string
		Secret   string
	}
	Google struct {
		ClientId string
		Secret   string
	}
}

type VersionConf struct {
	APIVersion    string `json:"api_version"`
	ServerVersion string `json:"server_version"`
}

type SecurityConf struct {
	AppToken  string
	PageToken string
}

type FacebookConf struct {
	AppToken  string
	PageToken string
	AppSecret string
	AppId     string
}

type ServerConf struct {
	Url struct {
		Prod string
		Dev  string
		Test string
	}
	RunMode   string
	ProdPort  string
	TestPort  string
	ForceMode bool
	DebugMsg  bool
	Job       struct {
	}

	Work struct {
		EnableCancel    bool
		CancelWorkLimit int64

		EnableLimitWorkHour bool
		MaxWorkingHours     int64
	}
	Maintenance struct {
		Enable  bool
		Message string
	}
	Notification struct {
		Enable bool
	}
}

//type CacheConf struct {
//	Enable bool
//	System string
//	Server string
//	User   string
//	Pass   string
//	Url    string
//	Db     string
//}
//
//type RedisCache struct {
//	User string
//	Pass string
//	Url  string
//	Db   string
//}

type DbConf struct {
	Conn     string
	Server   string
	Debug    bool
	User     string
	Password string
	Port     string
	Db       string
	Host     string
}

//
//type ApiConf struct {
//	Server       string
//	Logic        string
//	Notification string
//	Task         string
//}

//
//type AwsConf struct {
//	Pub    string
//	Sec    string
//	Bucket string
//	File   struct {
//		Path string
//	}
//	Image struct {
//		Path string
//	}
//}

//endregion Config definition

func LoadConfig() {
	LoadConfigFrom("")
}

func LoadConfigFrom(path string) {
	if len(path) == 0 {
		currentDir, err := os.Getwd()
		if err != nil {
			panic(err.Error())
		}
		path = currentDir
	}

	//region Load server configuration
	// serverConf := viper.New()
	// serverConf.SetConfigName("server")       // name of config file (without extension)
	// serverConf.AddConfigPath(path + "/config-asset") // optionally look for config in the working directory
	// if err := serverConf.ReadInConfig(); err != nil {
	// 	panic(fmt.Errorf("Fatal errors config file: %s \n", err))
	// }
	// serverConf.WatchConfig()
	// serverConf.OnConfigChange(func(e fsnotify.Event) {
	// 	LoadConfig()
	// 	return
	// })
	// Conf.Server.Url.Prod = serverConf.GetString("url.prod")
	// Conf.Server.Url.Dev = serverConf.GetString("url.dev")
	// Conf.Server.Url.Test = serverConf.GetString("url.test")
	// Conf.Server.RunMode = serverConf.GetString("run_mode")
	// Conf.Server.ProdPort = serverConf.GetString("prod_port")
	// Conf.Server.TestPort = serverConf.GetString("test_port")
	// Conf.Server.ForceMode = serverConf.GetBool("force_mode")
	// Conf.Server.DebugMsg = serverConf.GetBool("debug_msg")
	// Conf.Server.Work.EnableCancel = serverConf.GetBool("work.enable_cancel")
	// Conf.Server.Work.CancelWorkLimit = serverConf.GetInt64("work.cancel_work_limit")
	// Conf.Server.Work.EnableLimitWorkHour = serverConf.GetBool("work.enable_limit_work_hour")
	// Conf.Server.Work.MaxWorkingHours = serverConf.GetInt64("work.max_working_hours")
	// Conf.Server.Maintenance.Enable = serverConf.GetBool("maintenance.enable")
	// Conf.Server.Maintenance.Message = serverConf.GetString("maintenance.message")
	// Conf.Server.Notification.Enable = serverConf.GetBool("notification.enable")

	//endregion

	//region Load security configuration
	// securityConf := viper.New()
	// securityConf.SetConfigName("security")             // name of config file (without extension)
	// securityConf.AddConfigPath(path + "/config-asset") // optionally look for config in the working directory
	// if err := securityConf.ReadInConfig(); err != nil {
	// 	panic(fmt.Errorf("Fatal errors config file: %s \n", err))
	// }
	// securityConf.WatchConfig()
	// securityConf.OnConfigChange(func(e fsnotify.Event) {
	// 	LoadConfig()
	// 	return
	// })

	// Conf.Security.AppToken = securityConf.GetString("app_token")
	// Conf.Security.PageToken = securityConf.GetString("page_token")
	//endregion Load security configuration

	//region Load facebook configuration
	facebookConf := viper.New()
	facebookConf.SetConfigName("facebook")             // name of config file (without extension)
	facebookConf.AddConfigPath(path + "/config-asset") // optionally look for config in the working directory
	if err := facebookConf.ReadInConfig(); err != nil {
		panic(fmt.Errorf("Fatal errors config file: %s \n", err))
	}
	facebookConf.WatchConfig()
	facebookConf.OnConfigChange(func(e fsnotify.Event) {
		LoadConfig()
		return
	})

	Conf.Facebook.AppToken = facebookConf.GetString("app_token")
	Conf.Facebook.PageToken = facebookConf.GetString("page_token")
	Conf.Facebook.AppId = facebookConf.GetString("app_id")
	Conf.Facebook.AppSecret = facebookConf.GetString("app_sec")
	//endregion Load facebook configuration

	//region Load social configuration
	// socialConf := viper.New()
	// socialConf.SetConfigName("social")       // name of config file (without extension)
	// socialConf.AddConfigPath(path + "/config-asset") // optionally look for config in the working directory
	// if err := socialConf.ReadInConfig(); err != nil {
	// 	panic(fmt.Errorf("Fatal errors config file: %s \n", err))
	// }
	// socialConf.WatchConfig()
	// socialConf.OnConfigChange(func(e fsnotify.Event) {
	// 	LoadConfig()
	// 	return
	// })
	// Conf.Social.Facebook.ClientId = socialConf.GetString("facebook.clientId")
	// Conf.Social.Facebook.Secret = socialConf.GetString("facebook.secret")
	// Conf.Social.Google.ClientId = socialConf.GetString("google.clientId")
	// Conf.Social.Google.Secret = socialConf.GetString("google.secret")
	//endregion Load social configuration

	//region Load database configuration
	// dbConf := viper.New()
	// dbConf.SetConfigName("db")           // name of config file (without extension)
	// dbConf.AddConfigPath(path + "/config-asset") // optionally look for config in the working directory
	// if err := dbConf.ReadInConfig(); err != nil {
	// 	panic(fmt.Errorf("Fatal errors config file: %s \n", err))
	// }
	// dbConf.WatchConfig()
	// dbConf.OnConfigChange(func(e fsnotify.Event) {
	// 	LoadConfig()
	// 	return
	// })
	// Conf.Db.Conn = dbConf.GetString("conn")
	// Conf.Db.Debug = dbConf.GetBool("debug")
	// Conf.Db.Server = Conf.Server.RunMode
	// if !Conf.Server.ForceMode {
	// 	Conf.Db.Server = dbConf.GetString("server")
	// }

	// Conf.Db.User = dbConf.GetString(Conf.Db.Server + ".user")
	// Conf.Db.Password = dbConf.GetString(Conf.Db.Server + ".password")
	// Conf.Db.Port = dbConf.GetString(Conf.Db.Server + ".port")
	// Conf.Db.Db = dbConf.GetString(Conf.Db.Server + ".db")
	// Conf.Db.Host = dbConf.GetString(Conf.Db.Server + ".host")
	//endregion Load database configuration

	//region Load AWS configuration
	//awsConf := viper.New()
	//awsConf.SetConfigName("aws")          // name of config file (without extension)
	//awsConf.AddConfigPath(path + "/config-asset") // optionally look for config in the working directory
	//if err := awsConf.ReadInConfig(); err != nil {
	//	panic(fmt.Errorf("Fatal errors config file: %s \n", err))
	//}
	//awsConf.WatchConfig()
	//awsConf.OnConfigChange(func(e fsnotify.Event) {
	//	LoadConfig()
	//	return
	//})
	//config-asset.Aws.Pub = awsConf.GetString("pub_key")
	//config-asset.Aws.Sec = awsConf.GetString("sec_key")
	//config-asset.Aws.Bucket = awsConf.GetString("bucket")
	//config-asset.Aws.File.Path = awsConf.GetString("file.path")
	//config-asset.Aws.Image.Path = awsConf.GetString("image.path")
	//endregion Load AWS configuration

	//region Load cache configuration
	//cacheConf := viper.New()
	//cacheConf.SetConfigName("cache")        // name of config file (without extension)
	//cacheConf.AddConfigPath(path + "/config-asset") // optionally look for config in the working directory
	//if err := cacheConf.ReadInConfig(); err != nil {
	//	panic(fmt.Errorf("Fatal errors config file: %s \n", err))
	//}
	//cacheConf.WatchConfig()
	//cacheConf.OnConfigChange(func(e fsnotify.Event) {
	//	LoadConfig()
	//	return
	//})
	//config-asset.Cache.Enable = cacheConf.GetBool("enable")
	//config-asset.Cache.System = cacheConf.GetString("system")
	//config-asset.Cache.Server = Conf.Server.RunMode
	//if !Conf.Server.ForceMode {
	//	Conf.Cache.Server = cacheConf.GetString("server")
	//}
	//
	//if Conf.Cache.System == "redis" {
	//	prefix := fmt.Sprintf("%s.%s", Conf.Cache.System, Conf.Cache.Server)
	//	Conf.Cache.User = cacheConf.GetString(prefix + ".user")
	//	Conf.Cache.Pass = cacheConf.GetString(prefix + ".pass")
	//	Conf.Cache.Url = cacheConf.GetString(prefix + ".url")
	//	Conf.Cache.Db = cacheConf.GetString(prefix + ".db")
	//}
	//endregion Load cache configuration

	//region Load version configuration
	// versionConf := viper.New()
	// versionConf.SetConfigName("version")      // name of config file (without extension)
	// versionConf.AddConfigPath(path + "/config-asset") // optionally look for config in the working directory
	// if err := versionConf.ReadInConfig(); err != nil {
	// 	panic(fmt.Errorf("Fatal errors config file: %s \n", err))
	// }
	// versionConf.WatchConfig()
	// versionConf.OnConfigChange(func(e fsnotify.Event) {
	// 	LoadConfig()
	// 	return
	// })
	// Conf.Version.APIVersion = versionConf.GetString("api_version")
	// Conf.Version.ServerVersion = versionConf.GetString("server_version")
	//endregion Load version configuration
}
