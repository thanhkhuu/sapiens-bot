package main

import (
	"fmt"

	"github.com/gomessenger/messenger"
	dl "gitlab.com/sapiens-bot/dialogflow-services"
)

const (
	// facebook messenger actions
	MARK_SEEN  = "mark_seen"
	TYPING_ON  = "typing_on"
	TYPING_OFF = "typing_off"
)

//processing received message
func ProcessMessage(bot *messenger.Messenger, event *messenger.Messaging, resp *messenger.Response) {
	resp.SendAction(MARK_SEEN)
	resp.SendAction(TYPING_ON)

	//catch message key word
	// if event.Message.Text == "search" {
	// } else {
	// 	resp.SendText(event.Message.Text)
	// }

	// Use NLP

	response := dl.Dp.ProcessNLP(event.Message.Text, "testUser")
	fmt.Printf("%#v", response)
	resp.SendText(fmt.Sprintf("your intent: %s, confidence: %f", response.Intent, response.Confidence))
	resp.SendAction(TYPING_OFF)

	// //POST method, receives a json to parse
	// body, err := ioutil.ReadAll(r.Body)
	// if err != nil {
	// 	http.Error(w, "Error reading request body",
	// 		http.StatusInternalServerError)
	// }
	// type inboundMessage struct {
	// 	Message string
	// }
	// var m inboundMessage
	// err = json.Unmarshal(body, &m)
	// if err != nil {
	// 	panic(err)
	// }
}

func ProcessPostBack(bot *messenger.Messenger, event *messenger.Messaging, resp *messenger.Response) {

}
